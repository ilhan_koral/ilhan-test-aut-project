# :snake: pytest-bdd Test Automation Project
>This repository is created for **JUMPSTART** program of **[ttl.be](https://www.ttl.be/jumpstart-program)** and contains example code for the *Behavior-Driven Python with **pytest-bdd***
One of the main learning outcome of this project is figuring out how to create, explore, understand, and run bdd test environment. So even if I manage to pass all of the tests in a couple days,
many times I went back and practice runing steps file pairs with fature file. I have finished pytest tutorial **[here](https://testautomationu.applitools.com/behavior-driven-python-with-pytest-bdd/)** 
and red through related sections in this **[documantation](https://docs.pytest.org/en/latest/contents.html)**. Then improve and add new feature to my project. Whenever I learnt new things or meet 
challange I tried to made it in different branch and different commit. The challenges was setting enviroment correctly with dependencies
keeping system stable adding new component and packets, running the tests using my fixture, POM and main configuration file. Another challange was finding my way through this application to 
play with DOM and manage complicated or loose structured html elements.

---
## Index
* [Learning Objectives and Supported Skills](#markdown-header-learning-objectives-and-supported-skills)
* [Test Environment](#markdown-header-test-environment)
	* [System Under Test](#markdown-header-1-system-under-test)
	* [Sample User Stories](#markdown-header-2-sample-user-stories)
	* [Acceptance Criterieas](#markdown-header-3-acceptance-criterieas)
* [Technology-Tool-Stack](#markdown-header-technology-and-tool-stack)
* [Project Structure](#markdown-header-project-structure)
* [Setup](#markdown-header-setup)

---

## Learning Objectives and Supported Skills
* Debugging pytest Python code in VSC
* Running tests from the terminal
* Building html test report & logs
* Understanding what "bdd" means
* Working with feature ,fixture and POM
* Navigating larger directory structures
* Exploring and understanding DOM

---
## Test Environment
### 1. System Under Test

**[sportpartner](https://www.sportpartner.com/be-en)** web site is used for test. This web site has more than 250.000 members and can be good example for both well or loose structured elements. Morover signup process consists of several steps which makes
signup via automation challanging.


### 2. Sample User Stories
	
* As a user, I would like to navigate main page and can choose different language options, so that I can see content according to my language preferences.
* As a user, I would like to signup, so that I can create my profile and see other users
* As a user, I would like to click another user profile, so that I can see other user profile detail and see send message buttons.

### 3. Acceptance Criterieas

>#### AC For User Story 1:

* When I visit page guided information should be visible, including title, menu, language options
* On the main menu I should have option different language 
	* EN
	* NL
	* FR

* When I click language button I should see content accordingly
	* Login and Signup Button text
	* Explanations about page
	* Find your match button will be visible according to language

>#### AC For User Story 2:

* I am on signup page (1.step),
* if I left blank any area it shows me error message
	* left different area empty
	* then warning message should be appear
* If I  select all required areas and click next I should be able to see profile text and profile photo upload page,
* If I enter at least 20 characters I can go further to (2. Step) and web page should ask me; whether I will upload a photo or not and uploading photo should be optional,
* If I skip upload photo I should be directed to premium user offer page (3. Step),
* Finally I skip premium user offer I should be access dashboard (4.step) and can see other profiles.

>#### AC For User Story 3:

* I am logged on, and on dashboard page (member page which only logged member can see),
* When I click first user “full profile” link I should be directed to user profile page,
* Below elements should be seen;
	* about me text,
	* avatar picture (jpg format)
* I should see;
	* start conversation button
	* give high five button.

---
## Technology and Tool Stack*

Tool				| Name	 								|
---------:			| :----- 								|
Operating System  	| Ubuntu 19.10 (Virtual Box) 			|
Language     		| Python 3.7.5							|
IDE      			| Visual Studio Code					|
Framework			| pytest 5.4.1							|
Plugins				| pytest-bdd, pytest-html, pytest-xdist	|


*This tools are used inside venv virtual enviroment to make sure dependency is correctly set up.

---
## Project Structure

* :file_folder: ** my_project **
    * -- README.md
    * -- config.json
    * -- pytest.ini
    * -- report.html
    * :file_folder: ** assets **
        * -- style.css
    * :file_folder: ** tests **
        * -- conftest.py
        * :file_folder: ** features **
            * -- web_ac01.feature
            * -- web_ac02.feature
        * :file_folder: ** pages **
            * -- __init__.py
            * -- home.py
            * -- signUp.py
        * :file_folder: ** step_defs **
            * -- __init__.py
            * -- test_web_ac01_steps.py
            * -- test_web_ac02_steps.py

---
## Setup

#### Prerequisites
This project requires an Python 3.7 or higher , pip package manager installed. It also uses venv environment to manage packages. 
It has been assummed chrome, firefox browser and selenium web driver are installaed and PATH variable is set.

To set up this project on your local machine:

#### Create Virtual Environment
 * Install python3-venv package if you have not installed yet.

   `ikoral@ikoral-ttl:~/repos$ sudo apt-get install python3-venv`
   
 * Create virtual enviroment

   `ikoral@ikoral-ttl:~/repos$ python3.7 -m venv my_env`

 * Activate virtual enviroment

   `ikoral@ikoral-ttl:~/repos$ source my_env/bin/activate`
   
 * Goto virtual enviroment directory

   `(my_env) ikoral@ikoral-ttl:~/repos$ cd my_env`  


#### Install Packages & Dependencies
 * Clone the project from this repository.
   `(my_env) ikoral@ikoral-ttl:~/repos/my_env$ git clone ...`
 
* `pip install pytest` pytest core
* `pip install pytest-bdd` bdd plug-in
* `pip install pytest-html` report plug-in
* `pip install pytest-xdist` parallel test plug-in

#### Open Project and Run The Tests
* Goto project directory:
    * `(my_env) ikoral@ikoral-ttl:~/repos/my_env$ cd ilhan-test-aut-project`
* Open Editor in the project directory:
    * `(my_env) ikoral@ikoral-ttl:~/repos/my_env/ilhan-test-aut-project$ code .`
* Open your terminal and run test 
    * `pytest -v`
   
For detailed command and command flags please refer to pytest **[documentation](https://docs.pytest.org/en/latest/contents.html)**.




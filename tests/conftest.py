# import sys, os
# myPath = os.path.dirname(os.path.abspath(__file__))
# sys.path.insert(0, myPath + '/../')

import pytest
import json

from pytest_bdd import given
from selenium import webdriver
from selenium.webdriver import Chrome, Firefox

# ----------Constants-----------------

SPORTPARTNER_HOME = 'https://www.sportpartner.com/be'

SPORTPARTNER_HOME_EN = 'https://www.sportpartner.com/be-en'

# -----------Hooks----------------------

def pytest_bdd_step_error(request, feature, scenario, step, step_func, step_func_args, exception):
    print(f'Step failed: {step}')
    print(f'Step failed: {step_func}')

# ----------Fixtures-----------------

# This define where the config file sit and scope for this file
@pytest.fixture(scope='session')
def config():
    with open('config.json') as config_file:
        data = json.load(config_file)
    return data


# This checks if test env browser correctly set up
@pytest.fixture(scope='session')
def config_browser(config):
    if 'browser' not in config:
        raise Exception('The config file does not contain browser')
    elif config['browser'] not in ['chrome', 'firefox']:
        raise Exception(f'"{config["browser"]}" is not a supported browser')
    return config['browser']


# adding explicitly wait tome from config, it there is not then add 10 seconds
@pytest.fixture(scope='session')
def config_wait_time(config):
    return config['wait_time'] if 'wait_time' in config else 10


# @pytest.fixture(scope='session')
@pytest.fixture
# This works before and after test
def browser(config_browser, config_wait_time):  # with quit
    if config_browser == 'chrome':
        # initialize ChromeDriver
        driver = Chrome()
    elif config_browser == 'firefox':
        driver = Firefox()
    else:
        raise Exception(f'"{config_browser}" is not a supported browser')

    # wait on purpose and make sure page loaded
    driver.implicitly_wait(config_wait_time)

    # Return the driver object at the end of the setup
    yield driver

    # cleanup
    driver.quit()


@pytest.fixture
def browser_continue():  # without quit
    # initialize ChromeDriver
    driver = Chrome()

    # wait on purpose and make sure page loaded
    driver.implicitly_wait(10)

    # Return the driver object at the end of the setup
    yield driver

# Shared Given Steps

@given('the sportpartner home page is displayed')
def sport_home(browser):
    browser.get(SPORTPARTNER_HOME)

@given('sportpartner english home page is displayed shared')
def sport_home_en(browser):
    browser.get(SPORTPARTNER_HOME_EN)


from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys


class Home:

    # LINK_DIVS = (By.CSS_SELECTOR, '#links > div')
    # SEARCH_INPUT = (By.ID, 'search_form_input')
    
    def __init__(self, browser):
        self.browser = browser

    def load(self, url):
        self.browser.get(url)

    def find_element_class_text(self, html_tag, class_name, text):

        xpath = f"//{html_tag}[contains(@class,'{class_name}')][contains(text(), '{text}')]"
        element = self.browser.find_element_by_xpath(xpath)
        return element

    def find_element_byId(self, id):
        element = self.browser.find_element_by_id(id)
        return element


    def element_click(self, element):
        element.click()

    def cleanup(self):
        self.browser.quit() 
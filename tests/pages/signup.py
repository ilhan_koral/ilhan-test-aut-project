from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from pages.home import Home


class SignUp(Home):

    def find_element_byhtc(self, html_tag=None, class_name=None, text=None):
        if html_tag is None and text is not None and class_name is None:  # only text
            xpath = f'//*[contains(text(), "{text}")]'
        elif html_tag is None and text is None and class_name is not None:  # only class_name
            xpath = f"//*[contains(@class,'{class_name}')]"
        elif html_tag is not None and text is not None and class_name is None:  # html_tag and text
            xpath = f'//{html_tag}[contains(text(), "{text}")]'
        elif html_tag is not None and text is None and class_name is not None:  # html_tag and class_name
            xpath = f"//{html_tag}[contains(@class,'{class_name}')]"
        elif html_tag is None and text is not None and class_name is not None:  # class_name and text
            xpath = f"//*[contains(@class,'{class_name}')][contains(text(), '{text}')]"
        elif html_tag is not None and text is not None and class_name is not None:  # html_tag and class_name and text
            xpath = f"//{html_tag}[contains(@class,'{class_name}')][contains(text(), '{text}')]"
        elements = self.browser.find_elements_by_xpath(xpath)
        return elements

    def find_element_panels(self):
        xpath_fields = "//div[@class='panel']/div[@class='field']"
        el_fields = self.browser.find_elements_by_xpath(xpath_fields)
        return el_fields

    # it searches element in root element for example if root element is <section>
    # it search element inside section. If target element dies not first chil the method
    # aslo eaccept one parent element For exemple parent <div> and child <p>
    def find_child_elements_by_xpath(self, child, root, child_class=None, parent=None):
        if parent is None and child_class is None:
            xpath = f".//{child}"
        elif parent is None and child_class is not None:
            xpath = f".//{child}[contains(@class,'{child_class}')]"
        elif parent is not None and child_class is None:
            xpath = f".//{parent}/{child}"
        elif parent is not None and child_class is not None:
            xpath = f".//{parent}/{child}[contains(@class,'{child_class}')]"
        elements = root.find_elements_by_xpath(xpath)
        return elements

    # className = 'select-sport-panel'
    # el_sport_labels = field.find_elements_by_css_selector('label[class*="%s"]' % className)

    # LINK_DIVS = (By.CSS_SELECTOR, '#links > div')
    # SEARCH_INPUT = (By.ID, 'search_form_input')
    # xpath_labels = ".//label[contains(@class, 'select-sport-panel')]"
    # xpath_checkbox = ".//div/input[@type='checkbox'][contains(@class, 'sport')]"

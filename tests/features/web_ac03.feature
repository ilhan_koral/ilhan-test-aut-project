@sportpartner @login
Feature: sportpartner access to matches (login)
  As a user,
  I would like to click another user profile,
  So that I can see other user profile detail and see send message buttons.


  Background:
    Given sportpartner english home page is displayed shared
    And   navigate to login page

  @login
  Scenario: Logged on and goto dashboard page
    When the user logon via email and password
    Then the user dashboard page is shown



@sportpartner
Feature: sportpartner language options
  As a user,
  I wouldlike to choose different language options
  So that I can see content according to my language preferences.


  Background:
    Given the sportpartner home page is displayed

  @language
  Scenario Outline: Select different language
    When the user select for "<lang>"
    Then results are shown as "<result>"

  Examples:
  | lang | result               |
  | en   | Together is more     |
  | nl   | Samen is meer        |
  | fr   | Ensemble c'est plus  |

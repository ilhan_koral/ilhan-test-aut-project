@sportpartner @signup
Feature: sportpartner create user profile (signup)
  As a user,
  I would like to signup,
  So that I can create my profile and see other users.


  Background:
    Given the sportpartner english home page is displayed

  @signup_page
  Scenario: Navigate Signup Page
    When the user choose signup via email
    Then member registration page is shown


  # -------------signup registration fields check--------
  @signup_sports
  Scenario: Sports type no selection
    When the user choose signup via email
    And  the user does not choose any sport and submit
    Then Error message is shown for sports


  @signup_sports
  Scenario: Sports type selection
    When the user choose signup via email
    And  the user choose at least one sport and submit
    Then Error message shouldn't be seen for sports


  # -------------signup fill form and submit-----------
  @signup_fill
  Scenario: Sportparner signup fill required sections
    When the user choose signup via email
    And  the user choose all required area and submit
    And  about yourself page should be seen

    And  the user enter about text and continue without photo
#   Then user sould access dashboard

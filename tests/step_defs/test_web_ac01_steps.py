"""
This module contains step definitions for web.feature.
It uses Selenium WebDriver for browser interactions:

Setup and cleanup are handled using hooks.
For a real test automation project,
use Page Object Model.

Prerequisites:
 - Chrome must be installed.
 - chromedriver must be installed and accessible on the system path.
"""
import time
from pytest_bdd import scenarios, when, then, parsers
from selenium.webdriver.common.keys import Keys

CONVERTERS = {
    'lang': str,
    'result': str,
}

# Scenarios

scenarios('../features/web_ac01.feature', example_converters=CONVERTERS)

# When Steps


# @when(parsers.cfparse("the user select for {lang}"))
@when('the user select for "<lang>"')
def select_language(browser, lang):
    xpath = "//a[contains(@class,'selectitem-language')][contains(text(),'" + lang + "')]"
    en_link = browser.find_element_by_xpath(xpath)
    en_link.click()
    time.sleep(1)

# Then Steps

@then('results are shown as "<result>"')
def language_results(browser, result):
    xpath = '//h1[contains(text(), "%s")]' % result
    results = browser.find_elements_by_xpath(xpath)
    print(result)
    assert len(results) > 0
    time.sleep(1)
    if (result == "Ensemble c'est plus"):
        browser.quit()
    

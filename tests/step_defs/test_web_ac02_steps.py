"""
This module contains step definitions for web.feature.
and uses Page Object Model.

"""

import pytest
import time
from pytest_bdd import scenarios, scenario, given, when, then, parsers
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from pages.signup import SignUp
from pages.home import Home
import random

URL = "https://www.sportpartner.com/be-en"


def global_page(browser):
    global home_page
    home_page = Home(browser)


# -------------SCENARIOS IMPORT-------------
# scenarios('../features/web_ac02.feature', example_converters=CONVERTERS)


@scenario('../features/web_ac02.feature', 'Navigate Signup Page')
def test_navigate_signup_page():
    pass


@scenario('../features/web_ac02.feature', 'Sports type no selection')
def test_no_sport_type_selected():
    pass


@scenario('../features/web_ac02.feature', 'Sports type selection')
def test_sport_type_selected():
    pass


@scenario('../features/web_ac02.feature', 'Sportparner signup fill required sections')
def test_signup_fill():
    pass


# @scenario('../features/web_ac02.feature', 'Sportparner fill about')
# def test_about_fill():
#     pass

# ------------GIVEN STEPS-------------
@given('the sportpartner english home page is displayed')
def load_sportpartner(browser):
    global_page(browser)
    home_page.load(URL)
    # home_page.cleanup()


# ------------WHEN STEPS-------------

# >>> Scenario > @signup_page >>>
# >>> Scenario > @signup_sports >>>
# >>> Scenario > @signup_sports >>>
@when('the user choose signup via email')
def navigate_signup(browser):
    global_page(browser)
    element = home_page.find_element_class_text(
        "a", "btn btn-link deep-sky-blue", "Sign up")
    home_page.element_click(element)
    time.sleep(0.5)

    # As an alternative we may execute JS script inside python for complex element without ID.
    javaScript = "document.querySelector('#sign-up > div > div > div > div.main-form > div:nth-child(7) > div > a').click();"
    home_page.browser.execute_script(javaScript)

    time.sleep(2)


# >>> Scenario > @signup_sports >>>
@when('the user does not choose any sport and submit')
def no_sport_choosen_submit(browser):
    # create SignUp page instance
    signup_page = SignUp(browser)

    # signup page structured 10 different main sections (called fields)
    el_fields = signup_page.find_element_panels()
    # 1st field is choosen
    field = el_fields[0]

    # In the SignUp Page some selector builder created we may use this method for our convenient
    # find selectable label elements inside 1st field only these label clickable
    # These labels childs of above field (1st field).
    el_sport_labels = signup_page.find_child_elements_by_xpath(
        'label', field, 'select-sport-panel')

    # check status of checkboxes whether their status checked or not
    for sport_label in el_sport_labels:
        sport_checkbox_list = signup_page.find_child_elements_by_xpath(
            "input[@type='checkbox']", sport_label, "sport", 'div')
        sport_checkbox = sport_checkbox_list[0]

        # if checkbox status checked == true set it false
        if sport_checkbox.is_selected():
            sport_label.click()

    # after make sure all sports area uncheked submit form
    button_list = signup_page.find_element_byhtc(
        'button', 'btn btn-round btn-submit', 'Sign up')
    button = button_list[0]
    button.click()
    time.sleep(2)


# >>> Scenario > @signup_sports >>>
@when('the user choose at least one sport and submit')
def sport_choosen_submit(browser):
    signup_page = SignUp(browser)
    el_fields = signup_page.find_element_panels()
    field = el_fields[0]
    el_sport_labels = signup_page.find_child_elements_by_xpath(
        'label', field, 'select-sport-panel')

    counter = 0
    # check status of checkboxes whether their status checked or not
    for sport_label in el_sport_labels:
        sport_checkbox_list = signup_page.find_child_elements_by_xpath(
            "input[@type='checkbox']", sport_label, "sport", 'div')
        sport_checkbox = sport_checkbox_list[0]

        # if checkbox status checked == false set it true
        # if sport_checkbox.is_selected() == False:
        if counter == 0:
            sport_label.click()
        counter += 1

    button_list = signup_page.find_element_byhtc(
        'button', 'btn btn-round btn-submit', 'Sign up')
    button = button_list[0]
    button.click()
    time.sleep(2)

# >>> Scenario > @signup_fill >>>
@when('the user choose all required area and submit')
def signup_page_fill_registration(browser):
    helped_fill_signup_sports(browser)
    helped_fill_personal(browser)
    helped_submit_signup(browser)
    time.sleep(3)


# >>> Scenario > @signup_fill>>>
@when("about yourself page should be seen")
def about_yourself_page_shown(browser):
    signup_page = SignUp(browser)
    page_title = signup_page.browser.find_element_by_tag_name('title')
    title = page_title.get_attribute('innerText')

    element_p_list = signup_page.find_element_byhtc(
        'p', None, "You've taken the first step. Write something about yourself and upload a photo below.")

    if ('Welcome' in title and len(element_p_list) > 0):
        assert True
    else:
        assert False
    time.sleep(2)

# >>> Scenario > @signup_fill >>>
@when('the user enter about text and continue without photo')
def signup_page_fill_about(browser):
    helped_submit_about(browser)
    time.sleep(2)


# --------------THEN STEPS-----------------

# >>> Scenario > @signup_page >>>
@then('member registration page is shown')
def signup_page_shown(browser):
    element = home_page.browser.find_element_by_tag_name('title')
    elementHTML = element.get_attribute('innerText')
    if (elementHTML == 'Registration - Sportpartner'):
        assert True
    else:
        assert False

# >>> Scenario > @signup_sports >>>
@then('Error message is shown for sports')
def no_sport_choosen_error(browser):
    signup_page = SignUp(browser)
    element_list = signup_page.find_element_byhtc(
        None, None, 'Please select a number of sports')

    if (len(element_list) > 0):
        assert True
    else:
        assert False

# >>> Scenario > @signup_sports >>>
@then("Error message shouldn't be seen for sports")
def sport_choosen_no_error(browser):
    signup_page = SignUp(browser)
    element_list = signup_page.find_element_byhtc(
        None, None, 'Please select a number of sports')

    if (len(element_list) > 0):
        assert False
    else:
        assert True


# --------------HELPED METHODS FOR SIGNUP-----------------

def helped_fill_signup_sports(browser):
    # create SignUp page instance
    signup_page = SignUp(browser)

    # signup page structured 10 different main sections (called fields)
    el_fields = signup_page.find_element_panels()
    # 1st field is choosen
    field = el_fields[0]

    # chose related child elements to be filled inside 1st field
    el_sport_labels = signup_page.find_child_elements_by_xpath(
        'label', field, 'select-sport-panel')

    # generate random sports
    numbers = random_number_generator(3, 18)
    for i in numbers:
        el_sport_labels[i].click()


def helped_fill_personal(browser):
    signup_page = SignUp(browser)

    el_fields = signup_page.find_element_panels()
    # 3rd field is choosen for gender
    field = el_fields[2]
    el_gender_labels = signup_page.find_child_elements_by_xpath(
        'label', field, 'select-sport-panel')
    time.sleep(1)
    el_gender_labels[0].click()

    select = Select(signup_page.find_element_byId('select-age'))
    select.select_by_visible_text('30')

    address = signup_page.find_element_byId('Address')
    address.send_keys('Brussels')
    time.sleep(2)
    adress_panel = signup_page.find_element_byhtc(
        'div', 'pac-container pac-logo')
    child_adress_panel = signup_page.find_child_elements_by_xpath(
        'div', adress_panel[0], 'pac-item')
    child_adress_panel[0].click()

    user_name = 'testuser07'
    name = signup_page.find_element_byId('ScreenName')
    name.send_keys(user_name)

    email = signup_page.find_element_byId('Email')
    email.send_keys(user_name + '@membership.com')
    password = signup_page.find_element_byId('Password')
    password.send_keys(user_name)


def helped_submit_signup(browser):
    # create SignUp page instance
    signup_page = SignUp(browser)

    button_list = signup_page.find_element_byhtc(
        'button', 'btn btn-round btn-submit', 'Sign up')
    button = button_list[0]
    button.click()


def helped_submit_about(browser):
    signup_page = SignUp(browser)

    # find about profile textarea and enter at least 20 characters
    text_elements = signup_page.find_element_byhtc('textarea', 'form-control')
    text_elements[0].send_keys(
        "I like walking and go to nature. Looking for friends join to my adventure.")
    time.sleep(1)

    # submit about text
    button_list = signup_page.find_element_byhtc(
        'button', 'btn btn-round', 'Done')
    button = button_list[0]
    button.click()
    time.sleep(1)
    #after submitting new popup is shown for photo, skip photo upload
    button_without_photo = signup_page.find_element_byId('btnWithoutPhoto')
    button_without_photo.click()
    time.sleep(2)
    # after skipping photo premium offer is sugessted, skip premium
    link_list = signup_page.find_element_byhtc(
        'a', 'skip-introduction', 'Skip')
    link_skip = link_list[0]
    link_skip.click()



def random_number_generator(entries, scope):
    # create first number in numbers
    numbers = [None] * entries
    for i in range(entries):
        new_number = random.randint(0, scope)
        if i == 0:
            numbers[i] = new_number
        else:
            for j in numbers:
                while j == new_number:
                    new_number = random.randint(0, scope)
        numbers[i] = new_number
    return numbers



import pytest
import time
from pytest_bdd import scenarios, scenario, given, when, then, parsers
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from pages.login import Login


# -------------SCENARIOS-------------

@scenario('../features/web_ac03.feature', 'Logged on and goto dashboard page')
def test_login():
    pass


# ------------GIVEN STEPS-------------

# >>> Scenario > @login >>>
@given('navigate to login page')
def navigate_login(browser):
    # create Login page instance
    login_page = Login(browser)
    btn_list = login_page.find_element_byhtc(
        'a', 'btn btn-link outline-white-four', 'Login')
    btn_login = btn_list[0]
    btn_login.click()


# ------------WHEN STEPS-------------

# >>> Scenario > @login >>>
@when('the user logon via email and password')
def enter_credential(browser):
    login_page = Login(browser)
    email = login_page.find_element_byId('Email')
    email.send_keys('izleogrenkodla@gmail.com')
    password = login_page.find_element_byId('Password')
    password.send_keys('123456789')
    time.sleep(1)
    submit = login_page.find_element_byId('btn-login-email')
    submit.click()
    time.sleep(1)


# --------------THEN STEPS-----------------

# >>> Scenario > @login >>>
@then('the user dashboard page is shown')
def dashboard_page_shown(browser):
    login_page = Login(browser)
    avatar = login_page.find_element_byhtc(
        'button', 'hd-dropdown-avartar btn btn-link dropdown-toggle')
    if (len(avatar) > 0):
        assert True
    else:
        assert False
    time.sleep(5)



# --------BELOW CODE MAY REQUIRES FOR PROFILE OPERATIONS-----------
# dashboard
# <button type="button" class="m-0 p-0 hd-dropdown-avartar btn btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
#                         <span class="oval-avatar">
#                             <img class="avatar" src="https://files.sportpartner.com/assets/images/default-match-photo-male.svg">
#                         </span>
#                     </button>


# <div class="dropdown-menu dropdown-menu-right dropdown-menu-account show" x-placement="bottom-end" style="position: absolute; transform: translate3d(-258px, 42px, 0px); top: 0px; left: 0px; will-change: transform;">
#                         <div class="title-item-account">
#                             <div class="block-avatar">
#                                 <div class="block-img">
#                                     <span class="oval-avatar">
#                                         <img src="https://files.sportpartner.com/assets/images/default-match-photo-male.svg" alt="testuser01">
#                                     </span>
#                                 </div>
#                                 <div class="bl-name">
#                                     <h3 class="title">
#                                         testuser01
#                                     </h3>

#                                         <small>Basic member</small>
#                                 </div>
#                             </div>
#                                     <a class="menu-upgrade-button btn deep-sky-blue text-left go-to-package" data-tracking-id="headerGoPremium" href="/be-en/payment/options?src=MenuButton">
#                                         <img src="https://files.sportpartner.com/assets/images/icons-solid/premium-small.svg" alt="">Premium = more chance
#                                     </a>
#                         </div>
#                         <a class="dropdown-item" href="/be-en/member">Profile</a>
#                         <a class="dropdown-item" href="/be-en/member/settings">Settings</a>
#                         <a class="dropdown-item" href="/be-en/faq">Customer Service</a>
#                         <div class="dropdown-divider" style="margin-top: 5px; margin-bottom: 3px"></div>
#                         <a class="dropdown-item" href="/be-en/account/logout">Log out</a>
#                     </div>


# <title>Matches</title>
